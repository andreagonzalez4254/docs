---
title: How to install Database Lab with Terraform on AWS
sidebar_label: Install Database Lab with Terraform on AWS
---

Terraform is no longer a supported method for installing DLE.
